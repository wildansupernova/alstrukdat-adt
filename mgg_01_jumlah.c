/**
 * NIM       : 13516030
 * Nama      : Yonas Adiel Wiguna
 * Tanggal   : 24 Agustus 2017
 * Topik     : Pengenalan C
 * Deskripsi : Menjumlahkan 3 integer
 */

#include "stdio.h"

int main() {
	int a, b, c, sum;
	
	scanf("%d %d %d", &a, &b, &c);
	
	sum = a + b + c;
	
	printf("%d\n", sum);
	
	return 0;
}
