/* NIM       : 13516030 */
/* Nama      : Yonas Adiel Wiguna */
/* Tanggal   : 31 Agustus 2017 */
/* Topik     : ADT */
/* Nama File : mjam.c */
/* Deskripsi : Memberikan selisih detik antara dua jam */

#include "stdio.h"
#include "jam.h"

int main() {
	
	/* KAMUS */
	int N;           /* Jumlah record */
	int N_i;         /* iterasi */
	JAM J1, J2;      /* jam masukan */
	JAM JMin, JMax;  /* jam paling minimum dan jam paling maksimum */
	long durasi;     /* durasi antar masukan jam */
	
	scanf("%d",&N);
	for (N_i = 1; N_i <= N; N_i++) {
		printf("[%d]\n", N_i);
		BacaJAM(&J1);
		BacaJAM(&J2);
		
		/* Mengupdate data jam terminimum dan termaksimum */
		if (N_i == 1) {
			JMin = (JLT(J1, J2)) ? J1 : J2;
			JMax = (JGT(J1, J2)) ? J1 : J2;
		} else /* N_i != 1 */ {
			if (JLT(J1,JMin)) { JMin  = J1; }
			if (JLT(J2,JMin)) { JMin  = J2; }
			
			if (JGT(J1,JMax)) { JMax  = J1; }
			if (JGT(J2,JMax)) { JMax  = J2; }
		}
		
		if (JLT(J1, J2)) {
			/* Masukan jam pertama kurang dari masukan jam kedua */
			durasi = Durasi(J1, J2);
		} else /* JGT(J1,J2) || JEQ(J1, J2) */ {
			/* Masukan jam pertama lebih dari sama dengan masukan jam kedua */
			durasi = Durasi(J2, J1);
		}
		
		printf("%ld\n", durasi); 
	}
	
	TulisJAM(JMin); printf("\n");
	TulisJAM(JMax); printf("\n");
	
	return 0;
}
