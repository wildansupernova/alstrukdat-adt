#include "stdio.h"
#include "array.h"

int main() {
	
	TabInt T1, T2;
	IdxType i, j;
	ElType X, Y;

	printf("Membaca T1 :\n");
	printf("1. Membaca ukuran maks array sampai valid (>0)\n");
	printf("2. Membaca Neff sampai valid [0, maks]\n");
	printf("3. Membaca isi array sampai Neff\n");
	BacaIsi(&T1);
	printf("Isi T1 :\n"); TulisIsi(T1);

	printf("Membaca T2 :\n");
	printf("1. Membaca ukuran maks array sampai valid (>0)\n");
	printf("2. Membaca isi array sampai -9999\n");
	BacaIsiTab(&T2);
	printf("Isi T2 :"); TulisIsiTab(T2); printf("\n");
	printf("\n");

	printf("Nilai maximum pada T1 adalah %d di indeks %d\n", ValMax(T1), IdxMaxTab(T1));
	printf("Nilai minimum pada T1 adalah %d di indeks %d\n", ValMin(T1), IdxMinTab(T1));
	printf("Nilai maximum pada T2 adalah %d di indeks %d\n", ValMax(T2), IdxMaxTab(T2));
	printf("Nilai minimum pada T2 adalah %d di indeks %d\n", ValMin(T2), IdxMinTab(T2));
	printf("\n");

	if      (IsEQ(T1, T2))   { printf("T1 sama dengan T2\n"); }
	else if (IsLess(T1, T2)) { printf("T1 kurang dari T2\n"); }
	else                     { printf("T1 lebih dari T2\n"); } 
	if (IsSimetris(T1)) { printf("T1 simetris\n"); } else { printf("T1 tidak simetris\n"); }
	if (IsSimetris(T2)) { printf("T2 simetris\n"); } else { printf("T2 tidak simetris\n"); }
	printf("\n");

	printf("Elemen untuk ditambah di belakang T1 : "); scanf("%d", &X);
	AddAsLastEl(&T1, X);
	printf("Isi T1 sesudah ditambah :"); TulisIsiTab(T1); printf("\n");
	DelLastEl(&T1, &X);
	printf("Delete elemen terakhir T1 (%d): ", X); TulisIsiTab(T1); printf("\n");
	printf("\n");

	printf("Elemen untuk ditambah di T2 : "); scanf("%d", &X);
	do {
		printf("Letak element baru : "); scanf("%d",&i);
		if (!IsIdxEff(T2, i)) { printf("Indeks tidak valid\n"); }
	} while (IsIdxEff(T2, i));
	AddEli(&T2, X, i);
	printf("Isi T2 setelah ditambah :"); TulisIsiTab(T2); printf("\n");
	DelEli(&T2, i, &X);
	printf("Delete elemen ke-%d T2 (%d): ", i, X); TulisIsiTab(T2); printf("\n");
	printf("\n");

	MaxSortDesc(&T1);
	printf("Hasil Max Sort Desc T1 : "); TulisIsiTab(T1); printf("\n");
	T1 = InverseTab(T1);
	printf("Hasil Inverse Tab T1 : "); TulisIsiTab(T1); printf("\n");	
	InsSortAsc(&T2);
	printf("Hasil Insertion Sort Asc T2 : "); TulisIsiTab(T2); printf("\n");
	printf("\n");

	printf("Elemen untuk dicari secara urut : "); scanf("%d",&X);
	printf("Search Urut pada T1 menghasilkan %d\n", SearchUrut(T1, 6));
	printf("Search Urut pada T2 menghasilkan %d\n", SearchUrut(T2, X));
	printf("\n");

	printf("T1 punya nilai max %d dan min %d\n", MaxUrut(T1), MinUrut(T1));
	MaxMinUrut(T2, &X, &Y);
	printf("T2 punya nilai max %d dan min %d\n", X, Y);
	printf("\n");

	printf("Masukan elemen untuk ditambahkan secara urut : "); scanf("%d",&X);
	Add1Urut(&T1, X);
	Add1Urut(&T2, X);
	printf("Hasil T1 : "); TulisIsiTab(T1); printf("\n");
	printf("Hasil T2 : "); TulisIsiTab(T2); printf("\n");
	printf("Menghapus elemen %d secara urut :\n", X);
	Del1Urut(&T1, X);
	Del1Urut(&T2, X);
	printf("Hasil T1 : "); TulisIsiTab(T1); printf("\n");
	printf("Hasil T2 : "); TulisIsiTab(T2); printf("\n");
	printf("\n");

	printf("Masukan elemen untuk ditambahkan secara unik : "); scanf("%d", &X);
	printf("Menambahkan %d ke T1 secara unik\n", X); AddElUnik(&T1, X);
	printf("Menambahkan %d ke T2 secara unik\n", X); AddElUnik(&T2, X);
	printf("\n");

	printf("Mengopy T1 ke T2\n");
	CopyTab(T1, &T2);
	printf("Isi T1 sekarang : "); TulisIsiTab(T1); printf("\n");
	printf("Isi T2 sekarang : "); TulisIsiTab(T2); printf("\n");
	printf("\n");

	printf("Jika T1 dijumlah dengan T2 : "); TulisIsiTab(PlusTab(T1, T2)); printf("\n");
	printf("Jika T1 diikurangi dengan T2 : "); TulisIsiTab(MinusTab(T1, T2)); printf("\n");
	printf("Jika T1 dikali dengan T2 : "); TulisIsiTab(KaliTab(T1, T2)); printf("\n");
	printf("Masukan konstanta perkalian : "); scanf("%d",&X);
	printf("Jika T1 dikali dengan konstanta %d\n", X); TulisIsiTab(KaliKons(T1, X)); printf("\n");
	if (IsEQ(T1, T2)) { printf("T1 dan T2 sama\n");	}
	printf("\n");

	printf("Masukan elemen yang ingin dicari : "); scanf("%d",&X);
	if (SearchB(T1, X))        { printf("%d ada di T1 di indeks ke-%d\n", X, Search1(T1, X)); }
	else                       { printf("%d tidak ada di T1\n", X); }
	if (SearchSentinel(T2, X)) { printf("%d ada di T2 di indeks ke-%d\n", X, Search2(T2, X)); }
	else                       { printf("%d tidak ada di T2\n", X);}
	printf("\n");

	Dealokasi(&T1);
	Dealokasi(&T2);
	printf("T1 dan T2 sudah didealokasi\n");

	return 0;
}