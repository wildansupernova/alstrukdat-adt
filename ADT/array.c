/* MODUL TABEL INTEGER */
/* Berisi realisasi pemrosesan tabel integer */
/* Penempatan elemen selalu rapat kiri */
/* Versi I : dengan banyaknya elemen didefinisikan secara eksplisit, 
   memori tabel statik */

#ifndef ARRAY_H
#define ARRAY_H

#include "boolean.h"
#include "stdio.h"
#include "array.h"

/*  Kamus Umum */
#define IdxMax 100
/* Indeks maksimum array, sekaligus ukuran maksimum array dalam C */
#define IdxMin 1
/* Indeks minimum array */
#define IdxUndef -999 
/* Indeks tak terdefinisi*/

/* Definisi elemen dan koleksi objek */
typedef int IdxType;  /* type indeks */
typedef int ElType;   /* type elemen tabel */
typedef struct { 
  ElType TI[IdxMax+1]; /* memori tempat penyimpan elemen (container) */
  int Neff; /* >=0, banyaknya elemen efektif */
} TabInt;
/* Indeks yang digunakan [IdxMin..IdxMax] */
/* Jika T adalah TabInt, cara deklarasi dan akses: */
/* Deklarasi : T : TabInt */
/* Maka cara akses: 
   T.Neff  untuk mengetahui banyaknya elemen 
   T.TI    untuk mengakses seluruh nilai elemen tabel 
   T.TI[i] untuk mengakses elemen ke-i */
/* Definisi : 
  Tabel kosong: T.Neff = 0
  Definisi elemen pertama : T.TI[i] dengan i=1 
  Definisi elemen terakhir yang terdefinisi: T.TI[i] dengan i=T.Neff */
  
/* ********** SELEKTOR ********** */
#define Neff(T)   (T).Neff
#define TI(T)     (T).TI
#define Elmt(T,i) (T).TI[(i)]

/* ********** KONSTRUKTOR ********** */
/* Konstruktor : create tabel kosong  */
void MakeEmpty (TabInt * T)
/* I.S. T sembarang */
/* F.S. Terbentuk tabel T kosong dengan kapasitas IdxMax-IdxMin+1 */
{
  Neff(*T) = 0;
}

/* ********** SELEKTOR (TAMBAHAN) ********** */
/* *** Banyaknya elemen *** */
int NbElmt (TabInt T)
/* Mengirimkan banyaknya elemen efektif tabel */
/* Mengirimkan nol jika tabel kosong */
{
  return Neff(T);
}

/* *** Daya tampung container *** */
int MaxNbEl (TabInt T)
/* Mengirimkan maksimum elemen yang dapat ditampung oleh tabel */
{
  return IdxMax-IdxMin+1;
}
/* *** Selektor INDEKS *** */
IdxType GetFirstIdx (TabInt T)
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks elemen T pertama */
{
  return IdxMin;
}
IdxType GetLastIdx (TabInt T)
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks elemen T terakhir */
{
  return IdxMin + Neff(T) - 1;
}

/* ********** Test Indeks yang valid ********** */
boolean IsIdxValid (TabInt T, IdxType i)
/* Mengirimkan true jika i adalah indeks yang valid utk ukuran tabel */
/* yaitu antara indeks yang terdefinisi utk container*/
{
  return (i >= IdxMin && i <= IdxMax);
}
boolean IsIdxEff (TabInt T, IdxType i)
/* Mengirimkan true jika i adalah indeks yang terdefinisi utk tabel */
/* yaitu antara FirstIdx(T)..LastIdx(T) */
{
  return (i >= GetFirstIdx(T) && i <= GetLastIdx(T));
}

/* ********** TEST KOSONG/PENUH ********** */
/* *** Test tabel kosong *** */
boolean IsEmpty (TabInt T)
/* Mengirimkan true jika tabel T kosong, mengirimkan false jika tidak */
{
  return NbElmt(T) == 0;
}
/* *** Test tabel penuh *** */
boolean IsFull (TabInt T)
/* Mengirimkan true jika tabel T penuh, mengirimkan false jika tidak */
{
  return NbElmt(T) == MaxNbEl(T);
}

/* ********** BACA dan TULIS dengan INPUT/OUTPUT device ********** */
/* *** Mendefinisikan isi tabel dari pembacaan *** */
void BacaIsi (TabInt * T)
/* I.S. T sembarang */
/* F.S. Tabel T terdefinisi */
/* Proses : membaca banyaknya elemen T dan mengisi nilainya */
/* 1. Baca banyaknya elemen diakhiri enter, misalnya N */
/*    Pembacaan diulangi sampai didapat N yang benar yaitu 0 <= N <= MaxNbEl(T) */
/*    Jika N tidak valid, tidak diberikan pesan kesalahan */
/* 2. Jika 0 < N <= MaxNbEl(T); Lakukan N kali: Baca elemen mulai dari indeks 
      IdxMin satu per satu diakhiri enter */
/*    Jika N = 0; hanya terbentuk T kosong */
{
  int N;
  IdxType i;

  MakeEmpty(T);

  do {
    scanf("%d",&N);
  } while (N < 0 || N > MaxNbEl(*T));
  Neff(*T) = N;

  for (i = GetFirstIdx(*T); IsIdxEff(*T, i); i++) {
    scanf("%d",&Elmt(*T, i));
  }

}

void BacaIsiTab (TabInt * T)
/* I.S. T sembarang */
/* F.S. Tabel T terdefinisi */
/* Proses : membaca elemen T sampai dimasukkan nilai -9999 */
/* Dibaca elemen satu per satu dan disimpan mulai dari IdxMin */
/* Pembacaan dihentikan jika pengguna memasukkan nilai -9999 */
/* Jika dari pertama dimasukkan nilai -9999 maka terbentuk T kosong */
{
  IdxType i;
  ElType  x;

  MakeEmpty(T);

  i = GetFirstIdx(*T);
  do {
    scanf("%d",&x);
    if (x != -9999) {
      Neff(*T) = Neff(*T) + 1;
      Elmt(*T, i) = x;
      i++;
    }
  } while (x != -9999 && !IsFull(*T));

}

void TulisIsi (TabInt T)
/* Proses : Menuliskan isi tabel dengan traversal */
/* I.S. T boleh kosong */
/* F.S. Jika T tidak kosong : indeks dan elemen tabel ditulis berderet ke bawah */
/*      Jika T kosong : Hanya menulis "Tabel kosong" */
/* Contoh: Jika isi Tabel: [1, 20, 30, 50]
   Maka tercetak di layar:
   [1]1
   [2]20
   [3]30
   [4]50
*/
{
  IdxType i;

  if (Neff(T) > 0) {
    for (i = GetFirstIdx(T); IsIdxEff(T, i); i++) {
      printf("[%d]%d\n", i, Elmt(T,i));
    }
  } else {
    printf("Tabel kosong\n");
  }
}

void TulisIsiTab (TabInt T)
/* Proses : Menuliskan isi tabel dengan traversal, tabel ditulis di antara kurung siku; 
   antara dua elemen dipisahkan dengan separator "koma", tanpa tambahan karakter di depan,
   di tengah, atau di belakang, termasuk spasi dan enter */
/* I.S. T boleh kosong */
/* F.S. Jika T tidak kosong: [e1,e2,...,en] */
/* Contoh : jika ada tiga elemen bernilai 1, 20, 30 akan dicetak: [1,20,30] */
/* Jika tabel kosong : menulis [] */
{
  IdxType i;

  printf("[");
  for (i = GetFirstIdx(T); IsIdxEff(T, i); i++) {
    if (i != GetFirstIdx(T)) printf(",");
    printf("%d", Elmt(T,i));
  }
  printf("]");
}

/* ********** OPERATOR ARITMATIKA ********** */
/* *** Aritmatika tabel : Penjumlahan, pengurangan, perkalian, ... *** */
TabInt PlusTab (TabInt T1, TabInt T2)
/* Prekondisi : T1 dan T2 berukuran sama dan tidak kosong */
/* Mengirimkan  T1+T2, yaitu setiap elemen T1 dan T2 pada indeks yang sama dijumlahkan */
{
  TabInt T3;
  IdxType i;

  MakeEmpty(&T3);
  Neff(T3) = Neff(T1);

  for (i = GetFirstIdx(T1); IsIdxEff(T1, i); i++) {
    Elmt(T3, i) = Elmt(T1, i) + Elmt(T2, i);
  }

  return T3;
}

TabInt MinusTab (TabInt T1, TabInt T2)
/* Prekondisi : T1 dan T2 berukuran sama dan tidak kosong */
/* Mengirimkan T1-T2, yaitu setiap elemen T1 dikurangi elemen T2 pada indeks yang sama */
{
  TabInt T3;
  IdxType i;

  MakeEmpty(&T3);
  Neff(T3) = Neff(T1);

  for (i = GetFirstIdx(T1); IsIdxEff(T1, i); i++) {
    Elmt(T3, i) = Elmt(T1, i) - Elmt(T2, i);
  }

  return T3;
}

TabInt KaliTab (TabInt T1, TabInt T2)
/* Prekondisi : T1 dan T2 berukuran sama dan tidak kosong */
/* Mengirimkan T1 * T2 dengan definisi setiap elemen dengan indeks yang sama dikalikan */
{
  TabInt T3;
  IdxType i;

  MakeEmpty(&T3);
  Neff(T3) = Neff(T1);

  for (i = GetFirstIdx(T1); IsIdxEff(T1, i); i++) {
    Elmt(T3, i) = Elmt(T1, i) * Elmt(T2, i);
  }

  return T3;
}

TabInt KaliKons (TabInt Tin, ElType c)
/* Prekondisi : Tin tidak kosong */
/* Mengirimkan tabel dengan setiap elemen Tin dikalikan c */
{
  TabInt Tout;

  MakeEmpty(&Tout);
  Neff(Tout) = Neff(Tin);

  IdxType i;
  for (i = GetFirstIdx(Tin); IsIdxEff(Tin, i); i++) {
    Elmt(Tout, i) = Elmt(Tin, i) * c;
  }

  return Tout;
}


/* ********** OPERATOR RELASIONAL ********** */
/* *** Operasi pembandingan tabel : < =, > *** */
boolean IsEQ (TabInt T1, TabInt T2)
/* Mengirimkan true jika T1 sama dengan T2 yaitu jika ukuran T1 = T2 dan semua elemennya sama */
{
  IdxType i;
  boolean equal = true;

  if (Neff(T1) == Neff(T2)) {
    i = GetFirstIdx(T1);
    while (IsIdxEff(T1, i) && equal) {
      if (Elmt(T1, i) != Elmt(T2, i)) {
        equal = false;
      }
      i++;
    }
    return equal;
  } else /*Neff(T1) != Neff(T2)*/ {
    return false;
  }

}

boolean IsLess (TabInt T1, TabInt T2)
/* Mengirimkan true jika T1 < T2, */
/* yaitu : sesuai dg analogi 'Ali' < Badu'; maka [0, 1] < [2, 3] */
{
  IdxType i;
  boolean less = false;
  boolean found = false;

  i = GetFirstIdx(T1);
  while (!found) {
    if (!IsIdxEff(T1, i) && !IsIdxEff(T2, i)) {
      less = false; /* T1 == T2 */
      found = true;
    } else if (!IsIdxEff(T1, i)) {
      less = true;
      found = true;
    } else if (!IsIdxEff(T2, i)) {
      less = false;
      found = true;
    } else if (Elmt(T1, i) != Elmt(T2, i)) {
      less = (Elmt(T1, i) < Elmt(T2, i));
      found = true;
    }

    i++;
  }
  return less;

}

/* ********** SEARCHING ********** */
/* ***  Perhatian : Tabel boleh kosong!! *** */
IdxType Search1 (TabInt T, ElType X)
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan indeks i terkecil, dengan elemen ke-i = X */
/* Jika tidak ada, mengirimkan IdxUndef */
/* Menghasilkan indeks tak terdefinisi (IdxUndef) jika tabel T kosong */
/* Memakai skema search TANPA boolean */
{
  IdxType IdxFound = IdxUndef;
  IdxType i        = GetFirstIdx(T);

  while (IsIdxEff(T, i) && IdxFound == IdxUndef) {
    if (Elmt(T, i) == X) {
      IdxFound = i;
    }
    i++;
  }

  return IdxFound;
}

IdxType Search2 (TabInt T, ElType X)
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan indeks i terkecil, dengan elemen ke-i = X */
/* Jika tidak ada, mengirimkan IdxUndef */
/* Menghasilkan indeks tak terdefinisi (IdxUndef) jika tabel T kosong */
/* Memakai skema search DENGAN boolean Found */
{
  IdxType IdxFound = IdxUndef;
  IdxType i        = GetFirstIdx(T);
  boolean found    = false;

  while (IsIdxEff(T, i) && !found) {
    if (Elmt(T, i) == X) {
      IdxFound = i;
      found = true;
    }
    i++;
  }

  return IdxFound;
}

boolean SearchB (TabInt T, ElType X)
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan true, jika tidak ada menghasilkan false */
/* Memakai Skema search DENGAN boolean */
{
  IdxType i        = GetFirstIdx(T);
  boolean found    = false;

  while (IsIdxEff(T, i) && !found) {
    if (Elmt(T, i) == X) {
      found = true;
    }
    i++;
  }

  return found;
}

boolean SearchSentinel (TabInt T, ElType X)
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan true, jika tidak ada menghasilkan false */
/* dengan metoda sequential search dengan sentinel */
/* Untuk sentinel, manfaatkan indeks ke-0 dalam definisi array dalam Bahasa C 
   yang tidak dipakai dalam definisi tabel */
{
  IdxType IdxFound = IdxUndef;
  IdxType i        = GetLastIdx(T);
  Elmt(T,0) = X;

  while (i >= 0 && IdxFound == IdxUndef) {
    if (Elmt(T, i) == X) {
      IdxFound = i;
    }
    i--;
  }

  return IdxFound != 0;
}

/* ********** NILAI EKSTREM ********** */
ElType ValMax (TabInt T)
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan nilai maksimum tabel */
{
  IdxType i = GetFirstIdx(T);
  ElType maxVal = Elmt(T, i);

  while (IsIdxEff(T, i)) {
    if (Elmt(T, i) > maxVal) {
      maxVal = Elmt(T, i);
    }
    i++;
  }

  return maxVal;
}

ElType ValMin (TabInt T)
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan nilai minimum tabel */
{
  IdxType i = GetFirstIdx(T);
  ElType maxVal = Elmt(T, i);

  while (IsIdxEff(T, i)) {
    if (Elmt(T, i) < maxVal) {
      maxVal = Elmt(T, i);
    }
    i++;
  }

  return maxVal;
}

/* *** Mengirimkan indeks elemen bernilai ekstrem *** */
IdxType IdxMaxTab (TabInt T)
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks i terkecil dengan nilai elemen merupakan nilai maksimum pada tabel */
{
  IdxType i = GetFirstIdx(T);
  IdxType maxIdx = i;

  while (IsIdxEff(T, i)) {
    if (Elmt(T, i) > Elmt(T, maxIdx)) {
      maxIdx = i;
    }
    i++;
  }

  return maxIdx;
}

IdxType IdxMinTab (TabInt T)
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks i terkecil dengan nilai elemen merupakan nilai minimum pada tabel */
{
  IdxType i = GetFirstIdx(T);
  IdxType minIdx = i;

  while (IsIdxEff(T, i)) {
    if (Elmt(T, i) < Elmt(T, minIdx)) {
      minIdx = i;
    }
    i++;
  }

  return minIdx;
}


/* ********** OPERASI LAIN ********** */
void CopyTab (TabInt Tin, TabInt * Tout)
/* I.S. Tin terdefinisi, Tout sembarang */
/* F.S. Tout berisi salinan dari Tin (elemen dan ukuran identik) */
/* Proses : Menyalin isi Tin ke Tout */
{
  IdxType i = GetFirstIdx(Tin);

  MakeEmpty(Tout);
  Neff(*Tout) = Neff(Tin);

  while (IsIdxEff(Tin, i)) {
    Elmt(*Tout, i) = Elmt(Tin, i);
    i++;
  }
}

TabInt InverseTab (TabInt T)
/* Menghasilkan tabel dengan urutan tempat yang terbalik, yaitu : */
/* elemen pertama menjadi terakhir, */
/* elemen kedua menjadi elemen sebelum terakhir, dst.. */
/* Tabel kosong menghasilkan tabel kosong */
{
  TabInt TInversed;
  IdxType i = GetFirstIdx(T);
  IdxType j = GetLastIdx(T);

  MakeEmpty(&TInversed);
  Neff(TInversed) = Neff(T);

  while (IsIdxEff(T, i)) {
    Elmt(TInversed, i) = Elmt(T, j);
    i++;
    j--;
  }

  return TInversed;
}

boolean IsSimetris (TabInt T)
/* Menghasilkan true jika tabel simetrik */
/* Tabel disebut simetrik jika: */
/*      elemen pertama = elemen terakhir, */
/*      elemen kedua = elemen sebelum terakhir, dan seterusnya */
/* Tabel kosong adalah tabel simetris */
{
  TabInt TInversed = InverseTab(T);
  return IsEQ(T, TInversed);
}

/* ********** SORTING ********** */
void MaxSortDesc (TabInt * T)
/* I.S. T boleh kosong */
/* F.S. T elemennya terurut menurun dengan Maximum Sort */
/* Proses : mengurutkan T sehingga elemennya menurun/mengecil */
/*          tanpa menggunakan tabel kerja */
{
  IdxType i,j,k;
  ElType X;

  for (i = GetFirstIdx(*T); IsIdxEff(*T, i); i++) {
    k = i;
    for (j = i; IsIdxEff(*T, j); j++) {
      if (Elmt(*T, k) < Elmt(*T, j)) {
        k = j;
      }
    }

    X = Elmt(*T, k);
    Elmt(*T, k) = Elmt(*T, i);
    Elmt(*T, i) = X;
  }
}

void InsSortAsc (TabInt * T)
/* I.S. T boleh kosong */
/* F.S. T elemennya terurut menaik dengan Insertion Sort */
/* Proses : mengurutkan T sehingga elemennya menaik/membesar */
/*          tanpa menggunakan tabel kerja */
{
  IdxType i,j;
  ElType X;

  for (i = GetFirstIdx(*T)+1; IsIdxEff(*T, i); i++) {
    j = i;
    while (j-1 >= GetFirstIdx(*T) && Elmt(*T, j-1) > Elmt(*T, j)) {
      X = Elmt(*T, j);
      Elmt(*T, j) = Elmt(*T, j-1);
      Elmt(*T, j-1) = X;

      j--;
    }
  }
}

/* ********** MENAMBAH ELEMEN ********** */
/* *** Menambahkan elemen terakhir *** */
void AddAsLastEl (TabInt * T, ElType X)
/* Proses: Menambahkan X sebagai elemen terakhir tabel */
/* I.S. Tabel T boleh kosong, tetapi tidak penuh */
/* F.S. X adalah elemen terakhir T yang baru */
{
  Neff(*T) = Neff(*T)+1;
  Elmt(*T, GetLastIdx(*T)) = X;
}

void AddEli (TabInt * T, ElType X, IdxType i)
/* Menambahkan X sebagai elemen ke-i tabel tanpa mengganggu kontiguitas 
   terhadap elemen yang sudah ada */
/* I.S. Tabel tidak kosong dan tidak penuh */
/*      i adalah indeks yang valid. */
/* F.S. X adalah elemen ke-i T yang baru */
/* Proses : Geser elemen ke-i+1 s.d. terakhir */
/*          Isi elemen ke-i dengan X */
{
  Neff(*T) = Neff(*T) + 1;

  ElType Y = X;
  ElType Temp;
  IdxType j = i;

  while (IsIdxEff(*T, j)) {
    Temp = Elmt(*T, j);
    Elmt(*T, j) = Y;
    Y = Temp;

    j++;
  }
}

/* ********** MENGHAPUS ELEMEN ********** */
void DelLastEl (TabInt * T, ElType * X)
/* Proses : Menghapus elemen terakhir tabel */
/* I.S. Tabel tidak kosong */
/* F.S. X adalah nilai elemen terakhir T sebelum penghapusan, */
/*      Banyaknya elemen tabel berkurang satu */
/*      Tabel T mungkin menjadi kosong */
{
  *X = Elmt(*T, GetLastIdx(*T));
  Neff(*T) = Neff(*T) - 1;
}

void DelEli (TabInt * T, IdxType i, ElType * X)
/* Menghapus elemen ke-i tabel tanpa mengganggu kontiguitas */
/* I.S. Tabel tidak kosong, i adalah indeks efektif yang valid */
/* F.S. X adalah nilai elemen ke-i T sebelum penghapusan */
/*      Banyaknya elemen tabel berkurang satu */
/*      Tabel T mungkin menjadi kosong */
/* Proses : Geser elemen ke-i+1 s.d. elemen terakhir */
/*          Kurangi elemen efektif tabel */
{
  IdxType j = i;

  *X = Elmt(*T, i);
  while (IsIdxEff(*T, j+1)) {
    Elmt(*T, j) = Elmt(*T, j+1);
    j++;
  }

  Neff(*T) = Neff(*T) - 1;
}

/* ********** TABEL DGN ELEMEN UNIK (SETIAP ELEMEN HANYA MUNCUL 1 KALI) ********** */
void AddElUnik (TabInt * T, ElType X)
/* Menambahkan X sebagai elemen terakhir tabel, pada tabel dengan elemen unik */
/* I.S. Tabel T boleh kosong, tetapi tidak penuh */
/*      dan semua elemennya bernilai unik, tidak terurut */
/* F.S. Jika tabel belum penuh, menambahkan X sbg elemen terakhir T, 
        jika belum ada elemen yang bernilai X. 
    Jika sudah ada elemen tabel yang bernilai X maka I.S. = F.S. 
    dan dituliskan pesan "nilai sudah ada" */
/* Proses : Cek keunikan dengan sequential search dengan sentinel */
/*          Kemudian tambahkan elemen jika belum ada */
{
  boolean hasX = SearchSentinel(*T, X);
  if (!hasX) {
    AddAsLastEl(T, X);
  } else {
    printf("nilai sudah ada\n");
  }
}

/* ********** TABEL DGN ELEMEN TERURUT MEMBESAR ********** */
IdxType SearchUrut (TabInt T, ElType X)
/* Prekondisi: Tabel T boleh kosong. Jika tidak kosong, elemen terurut membesar. */
/* Mengirimkan indeks di mana harga X dengan indeks terkecil diketemukan */
/* Mengirimkan IdxUndef jika tidak ada elemen tabel bernilai X */
/* Menghasilkan indeks tak terdefinisi (IdxUndef) jika tabel kosong */
{
  IdxType IdxFound = IdxUndef;
  IdxType i        = GetFirstIdx(T);

  while (IsIdxEff(T, i) && IdxFound == IdxUndef && Elmt(T, i) <= X) {
    if (Elmt(T, i) == X) {
      IdxFound = i;
    }
    i++;
  }

  return IdxFound;
}

ElType MaxUrut (TabInt T)
/* Prekondisi : Tabel tidak kosong, elemen terurut membesar */
/* Mengirimkan nilai maksimum pada tabel */
{
  return Elmt(T, GetLastIdx(T));
}

ElType MinUrut (TabInt T)
/* Prekondisi : Tabel tidak kosong, elemen terurut membesar */
/* Mengirimkan nilai minimum pada tabel*/
{
  return Elmt(T, GetFirstIdx(T));
}

void MaxMinUrut (TabInt T, ElType * Max, ElType * Min)
/* I.S. Tabel T tidak kosong, elemen terurut membesar */
/* F.S. Max berisi nilai maksimum T;
        Min berisi nilai minimum T */
{
  *Max = MaxUrut(T);
  *Min = MinUrut(T);
}

void Add1Urut (TabInt * T, ElType X)
/* Menambahkan X tanpa mengganggu keterurutan nilai dalam tabel */
/* Nilai dalam tabel tidak harus unik. */
/* I.S. Tabel T boleh kosong, boleh penuh. */
/*      Jika tabel isi, elemennya terurut membesar. */
/* F.S. Jika tabel belum penuh, menambahkan X. */
/*      Jika tabel penuh, maka tabel tetap. */
/* Proses : Search tempat yang tepat sambil geser */
/*          Insert X pada tempat yang tepat tersebut tanpa mengganggu keterurutan */
{
  IdxType i = GetFirstIdx(*T);

  if (!IsFull(*T)) {
    while (IsIdxEff(*T, i) && X > Elmt(*T, i)) {
      i++;
    }

    if (IsIdxEff(*T, i)) {
      AddEli(T, X, i);  
    } else {
      AddAsLastEl(T, X);
    }
    
  }
}

void Del1Urut (TabInt * T, ElType X)
/* Menghapus X yang pertama kali (pada indeks terkecil) yang ditemukan */
/* I.S. Tabel tidak kosong */
/* F.S. Jika ada elemen tabel bernilai X , */
/*      maka banyaknya elemen tabel berkurang satu. */
/*      Jika tidak ada yang bernilai X, tabel tetap. */
/*      Setelah penghapusan, elemen tabel tetap kontigu! */
/* Proses : Search indeks ke-i dengan elemen ke-i = X. */
/*          Delete jika ada. */
{
  IdxType i = SearchUrut(*T, X);
  if (i != IdxUndef) {
    DelEli(T, i, &X);
  }

}

#endif