/**
 * NIM       : 13516030
 * Nama      : Yonas Adiel Wiguna
 * Tanggal   : 24 Agustus 2017
 * Topik     : Pengenalan C
 * Deskripsi : Menghitung jumlah deret dari 1 sampai N
 */

#include "stdio.h"

int main() {
	int n, sum = 0, i;
	
	scanf("%d", &n);
	
	for (i = 1; i <= n; i++) { sum += i; }
	
	printf("%d\n", sum);
	
	return 0;
}
